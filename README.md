# hamster/mongodb

基于PHP7.1+ 实现的mongodb，优先使用mongodb/mongodb依赖，部分使用php7原生mongo写法。
mongodb/mongodb文档https://docs.mongodb.com/php-library/current/reference/

使用说明
-------
- 集合model需继承hamster\mongodb\Model，如test/Model/Hamster.php
- 类似常用数据库orm的链式查询方法，具体请看test/Example.php文件
- 开启集合id自增，对应model中设置protected $auto_increment = true;


错误提示
-------
- Trying to get property of non-object
> 没有执行DbManager::getInstance()初始化