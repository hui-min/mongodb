<?php

namespace hamster\mongodb;


trait Where
{
	public static $filter; // 筛选条件
	public static $options; // 命令选项
	public static $groupBy; // 分组字段

	/**
	 * 过滤条件
	 * @param array|object $filter
	 * @return $this
	 */
	public function where($filter)
	{
		self::$filter = $filter;

		return $this;
	}

	/**
	 * 模糊搜索条件
	 * @param string $column 字段
	 * @param string $keyword 关键字/正则表达式
	 * @return $this
	 */
	public function whereFuzzy($column, $keyword)
	{
		self::$filter[$column] = ['$regex'=>$keyword];

		return $this;
	}

	/**
	 * 字段
	 * @param array $columns 需要返回的字段
	 * @return $this
	 */
	public function field($columns=[])
	{
		foreach ($columns as $column) {
			self::$options['projection'][$column] = 1;
		}

		return $this;
	}

	/**
	 * 列表数量
	 * @param int $limit 列表数
	 * @return $this
	 */
	public function limit($limit)
	{
		self::$options['limit'] = $limit;

		return $this;
	}

	/**
	 * 排序
	 * @param array $sort [字段名 => 1-正序；-1-倒序] ['_id' => -1]
	 * @return $this
	 */
	public function sort($sort=[])
	{
		self::$options['sort'] = $sort;

		return $this;
	}

	/**
	 * 要跳过的文档数
	 * @param int $skip 跳过数量
	 * @return $this
	 */
	public function skip($skip=0)
	{
		self::$options['skip'] = $skip;

		return $this;
	}

	/**
	 * 文档分组
	 * @param string $groupBy 分组字段
	 * @return $this
	 */
	public function group($groupBy)
	{
		self::$groupBy = $groupBy;

		return $this;
	}
}