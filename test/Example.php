<?php
namespace hamster\mongodb\test;

use hamster\mongodb\test\Model\Hamster;

class Example
{
	// 列表查询
	public function find()
	{
		$model = new Hamster();
		$data = $model->where(['column1'=>1])->field(['_id', 'column1', 'column2'])->sort(['column1'=>-1])->skip(5)->limit(10)->find()->toArray();
	}

	// 分页查询
	public function paginate()
	{
		$model = new Hamster();
		$data = $model->where(['column1'=>1])->field(['_id', 'column1', 'column2'])->sort(['column1'=>-1])->skip(5)->limit(10)->paginate()->toArray();
	}
	
	// 查询某字段保存为一维数组
	public function pluck()
	{
		$model = new Hamster();
		$data = $model->where(['column1'=>1])->field(['column1', 'column2'])->pluck('column2', 'column1');

//		array(2) {
//			[1] => string(6) "张三"
//			[2] => string(6) "李四"
//		}
	}
	
	// 通道计算
	public function aggregateCount()
	{
		$model = new Hamster();
		$data = $model->where(['column1'=>1])->aggregateCount('sum', 'column1');

//		array(1) {
//		[0] => array(2) {
//			["_id"] => NULL
//			["num"] => int(209)
//		  }
//		}
	}

	// 连表查询
	public function lookup()
	{
		$model = new Hamster();
		$data = $model->where(['column1'=>2])->lookup('collection_b', 'hamster_id', '_id', 'info');

//		array(1) {
//			[0] => array(3) {
//				["_id"] => string(24) "609b9793817452566640e815"
//				["column"] => int(2)
//				["info"] => array(1) {
//					[0] => array(4) {
//						["_id"] => array(1) {
//							["$oid"] => string(24) "60a4b5d5024c0000a7000fa7"
//						}
//						["column"] => int(2)
//					}
//				}
//			}
//		}
	}
}